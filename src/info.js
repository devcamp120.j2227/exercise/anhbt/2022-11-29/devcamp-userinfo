import avatar from "./assets/images/avatardefault_92824.webp"
export const gUserInfo = {
  firstname: 'Hoang',
  lastname: 'Pham',
  avatar: avatar,
  age: 30,
  language: ['Vietnamese', 'Japanese', 'English'],
  isOld: function() {
    return this.age <= 35 ? "Anh ấy còn trẻ" : "Anh ấy đã già"
  }
}
