import {gUserInfo} from "./info"
function App() {
  return (
    <div>
      <h5>{gUserInfo.firstname} {gUserInfo.lastname}</h5>
      <img src={gUserInfo.avatar} alt="user avatar" width={300}></img>
      <p>{gUserInfo.age}</p>
      <p>{gUserInfo.isOld()}</p>
      <ul>{
        gUserInfo.language.map(function(element, index) {
          return <li key={index}>{element}</li>
        })
        }</ul>
    </div>
  );
}

export default App;
